<?php
require "selector.class.php";
require "irc.class.php";

$new = 0; $count = 0;
$filename = "/var/www/reddit-irc-notifier/last_post.db";
$last_post_text = file_get_contents ($filename);
$irc = new IRC();
$colors = $irc->colors();
$url = "https://reddit.com/r/forhire+jobbit+jobs4bitcoins/new";
$html = file_get_contents($url);

$dom = new SelectorDOM($html);
$links = $dom->select('p.title');
foreach($links as $link) {
    $count++;
    $title = $link["text"];
    $href = $link["children"][0]["attributes"]["href"];
    $full_link = "https://reddit.com".$href;
    echo $title."\n";
    echo $full_link."\n";
    if(!strstr(strtolower($title), "hiring")) continue;
    if( $title == $last_post_text) break;
    
    /* Got a new link */
    $new = true;
    
    file_put_contents($filename, $title);
    IRC::Alert("[".$colors->purple."Job".$colors->nc."] ". $title." ".$full_link, '#jobs');
    break;
}
echo "Parsed $count links and found $new new. \n\n";


?>
                
