## About reddit-irc-notification

reddit-irc-notification is a couple of small PHP scripts that parses https://reddit.com/r/forhire/new and sends an IRC notification for each new [Hiring] post, using a simple Python TCP relay bot.

## Deploying reddit-irc-notification

1. git clone https://gitlab.com/acorbin91/reddit-irc-notification
2. Edit echobot.py and edit lines 18-22 to match your IRCd location. Edit line 61 if you want your bot to identify with services.
3. Run python echobot.py to start your Python tcp relay bot. (tmux suggested for persistence)
4. Edit main.php and change line 26 to your desired notification channel.
5. Edit irc.class.php and change line 7 to match the hostname where your Python bot is running.
6. Run chmod last_post.db to ensure the script can write to that file.
7. Run with php main.php


## Add a cron job to run main.php every minute. (Optional)
<dd> * * * * * php /var/yourlocation/reddit-irc-notifier/main.php </dd>

## Questions?

Email [info@codebuilder.io](mailto:info@codebuilder.io) for questions or concerns.
