<?php
/* IRC Notification Bot Connector Class */
class IRC {
   //this function will connect to our python irc echobot
   public function alert($msg, $chan) {
        $cmd = "PRIVMSG ".$chan." :".$msg;
        $fp = fsockopen("tcp://localhost", 38232, $errno, $errstr, 30);
        if(!$cmd) return "no command given...";
        if(!$fp)  return "conn. refused";
                
        fwrite($fp, $cmd);
        //while (!feof($fp)) {
        //        $response .= fgets($fp, 128);
        //}
        fclose($fp);
   }
    public function colors() {  //this function will take color as text and then output the proper IRC color codes
        $this->nc = "0";
        $this->blue = "2";
        $this->green = "3";
        $this->lightred = "4";
        $this->red = "5";
        $this->purple = "6";
		$this->orange = "7";
        $this->yellow = "8";
        $this->lightgreen = "11";
        $this->lightblue = "12";
        $this->lightpurple = "13";
        $this->grey = "14";
        $this->lightgrey = "15";
        $this->darkwhite = "16";
		return $this;
   }


}
